<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddControlsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('controls', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_master_id')->unsigned()->nullable;
            $table->integer('donation_id')->unsigned()->nullable;
            $table->integer('zoneaffected_need_id')->unsigned()->nullable;
            $table->string('estado', 10)->default('1');
            $table->datetime('fecha_salida')->nullable;
            $table->timestamps();

            $table->foreign('user_master_id')->references('id')->on('users')
                        ->onDelete('cascade');
            $table->foreign('donation_id')->references('id')->on('donations')
                        ->onDelete('cascade');
            $table->foreign('zoneaffected_need_id')->references('id')->on                       ('zoneaffected_need')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('controls');
    }
}
