<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddZoneaffectedNeedTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('zoneaffected_need', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('need_id')->unsigned();
            $table->integer('zone_id')->unsigned();
            $table->timestamps();

            $table->foreign('need_id')->references('id')->on('needies');
            $table->foreign('zone_id')->references('id')->on('affectedzonies');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('zoneaffected_need');
    }
}
