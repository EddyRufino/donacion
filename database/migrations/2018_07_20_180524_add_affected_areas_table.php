<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAffectedAreasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('affectedzonies', function (Blueprint $table) {
            $table->increments('id');
            $table->string('adress');
            $table->integer('zone_id')->unsigned();
            $table->integer('user_id')->unsigned();
            //$table->integer('need_id')->unsigned();
            $table->string('estado', 10)->default('1');
            //$table->datetime('fecha')->nullable;


            $table->foreign('zone_id')->references('id')
                    ->on('cities')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users');
            //$table->foreign('need_id')->references('id')->on('needs');
            $table->timestamps();
        });

        // Schema::create('zoneaffected_need', function (Blueprint $table) {
        //     $table->increments('id');
        //     $table->integer('need_id')->unsigned();
        //     $table->integer('zone_id')->unsigned();
        //     // $table->integer('user_id')->unsigned();
        //     // $table->string('estado', 10)->default('1');
        //     // $table->datetime('fecha')->nullable;
        //     // $table->string('necesito', 50)->nullable;

        //     $table->foreign('need_id')->references('id')->on('needs');
        //     $table->foreign('zone_id')->references('id')->on('affecteds_areas');
        //     // $table->foreign('user_id')->references('id')->on('users');

        //     $table->timestamps();
        // });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('affectedzonies');
    }
}
