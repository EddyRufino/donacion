<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Need extends Model
{
    protected $table = 'needies';

    protected $fillable = ['name'];


    public function affected_areas()
    {
        return $this->belongsToMany('App\AffectedArea');
    }
}
