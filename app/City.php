<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $table = 'ciudad';

    protected $fillable = ['nombre'];

    public function Affected_Areas() 
    {
        return $this->hasMany('App\AffectedArea');
    }
}
