<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class zoneaffected_need extends Model
{
    protected $table = 'zoneaffected_need';

    protected $fillable = [
        'need_id',
        'zone_id'
    ];

    public function AffectedArea()
    {
        return $this->hasMany('App\AffectedArea');
    }

    public function Need()
    {
        return $this->hasMany('App\Need');
    }
}
