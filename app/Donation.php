<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Donation extends Model
{
    protected $table = 'donations';

    protected $fillable = [
        'name',
        'cantidad',
        'estado',
        'fecha_entrada',
        'num_serie',
        'unidad_medida',
        'category_id',
        'donor_id'
    ];

    public function Category()
    {
        return $this->belongsTo('App\Category');
    }

    public function Donor()
    {
        return $this->belongsTo('App\Donor');
    }

    public function controls()
    {
        return $this->hasMany('App\Control');
    }
}
