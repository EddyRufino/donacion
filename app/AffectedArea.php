<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AffectedArea extends Model
{
    protected $table = 'affectedzonies';

    protected $fillable = [
        'adress',
        'zone_id',
        'user_id'
    ];

    public function City()
    {
        return $this->belongsTo('App\City');
    }

    public function needs()
    {
        return $this->belongsToMany('App\Need');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
