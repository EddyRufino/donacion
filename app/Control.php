<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Control extends Model
{
    protected $table = 'controls';

    protected $fillable = [
        'user_master_id',
        'donation_id',
        'zoneaffected_need_id',
        'estado',
        'fecha_salida'
    ];

    public function Donation()
    {
        return $this->belongsTo('App\Donation');
    }

    public function User()
    {
        return $this->belongsTo('App\User');
    }
}
