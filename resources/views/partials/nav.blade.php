<ul class="nav d-flex justify-content-center">
    <li class="nav-item">
      <a class="nav-link active" href="{{route('users.index')}}">Usuarios</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="{{route('categories.create')}}">Categorias</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="#">Ciudad</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="#">Zona Afectada</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="#">Donación</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="#">Necesidad</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="#">Donador</a>
    </li>
    <li class="nav-item">
      <a class="nav-link disabled" href="#">Control</a>
    </li>
    <ul cclass="nav justify-content-end">
        <li><a href="#">Página Principal</a></li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" role="button" aria-haspopup="true" aria-expanded="false">Opciones <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="#">Salir</a></li>
          </ul>
        </li>
      </ul>
  </ul>
{{-- <nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <img src="" alt="">
    </div>

    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li><a href="#"></a>Usuarios</li>
        <li><a href="#">Categorias</a></li>
        <li><a href="#">Ciudad</a></li>
        <li><a href="#">Zona Afectada</a></li>
        <li><a href=""></a></li>
        <li><a href="#">Donación</a></li>
        <li><a href="#">Necesidad</a></li>
        <li><a href="#">Donador</a></li>
        <li><a href="#">Control</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="#">Página Principal</a></li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" role="button" aria-haspopup="true" aria-expanded="false">Opciones <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="#">Salir</a></li>
          </ul>
        </li>
      </ul>
    </div>
  </div>
</nav> --}}