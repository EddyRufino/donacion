@extends('layouts.app')

@section('title', 'Nueva Necesidad')

@section('content')
  {!! Form::open(['route' => 'needs.store', 'method' => 'POST']) !!}
    <div class="form-group">
      {!! Form::label('name', 'Nombre') !!}
      {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Nombre Completo','required' => 'true']) !!}
    </div>

    <div class="form-group">
      {!! Form::submit('Registrar', ['class' => 'btn btn-primary']) !!}
    </div>
  {!! Form::close() !!}
@endSection