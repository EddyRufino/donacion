@extends('admin.template.main')

@section('title', 'Crear Usuario')

@section('content')

  {!! Form::open(['route' => 'users.store', 'method' => 'POST']) !!}

    <div class="form-group">
      {!!Form::label('name', 'Nombre')!!}
      {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Nombre Completo','required' => 'true'])!!}
    </div>

    <div class="form-group">
      {!!Form::label('email', 'Email')!!}
      {!! Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'Example@gmail.com','required' => 'true'])!!}
    </div>

    <div class="form-group">
      {!!Form::label('password', 'Password')!!}
      {!! Form::password('password', ['class' => 'form-control', 'placeholder' => '**********','required' => 'true'])!!}
    </div>

    <div class="form-froup">
      {!! Form::label('type', 'Tipo') !!}
      {!! Form::select('type', ['' => 'Selecione un opción', 'member' => 'Miembro', 'admin' => 'Administrador'], null, ['class' => 'form-control']) !!}
    </div>
    <br>
    <div class="form-group">
      {!! Form::submit('Registrar', ['class' => 'btn btn-primary']) !!}
    </div>

  {!! Form::close() !!}

@endsection