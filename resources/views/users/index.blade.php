@extends('admin.template.main')

@section('title', 'Lista Usuarios')

@section('content')
  <a  href="{{route('users.create')}}" class="btn btn-info">Nuevo Usuario</a>
  <table class="table table-striped">
    <thead>
      <th>ID</th>
      <th>Nombre</th>
      <th>Email</th>
      <th>Tipo</th>
      <th>Anción</th>
    </thead>
    <tbody>
      @foreach($users as $user)
        <tr>
          <td>{{ $user->id }}</td>
          <td>{{ $user->name }}</td>
          <td>{{ $user->email }}</td>
          <td>
            @if($user->type == "admin")
              <span class="btn btn-danger">{{ $user->type }}</span>
            @else
              <span class="btn btn-primary">{{ $user->type }}</span>
            @endif
          </td>
          <td><a href="" class="btn btn-danger"></a> <a href="" class="btn btn-warning"></a></td>
        </tr>
      @endforeach
    </tbody>
  </table>

  {!! $users->render() !!}

@endsection