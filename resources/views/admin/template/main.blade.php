<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>@yield('title', 'INICIO') | ICPNA</title>
  <link rel="stylesheet" href="{{ asset('plugins/bootstrap/css/bootstrap.css') }}">
</head>
<body>

  @include('partials.nav')
  <br>
  <div class="container-fluid col-xs-12 col-sm-6 col-md-8 col-sm-offset-2">
    <h3>@yield('title')</h3>
    @yield('content')
  </div>

  <script src="{{ asset('plugins/jquery/js/jquery-2.1.4.js') }}" />
  <script src="{{ asset('plugins/bootstrap/js/bootstrap.js') }}" />
</body>
</html>