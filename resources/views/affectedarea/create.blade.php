@extends('layouts.app')

@section('title', 'Nueva Zona Afectada')

@section('content')
  {!! Form::open(['route' => 'affectedarea.store', 'method' => 'POST']) !!}
    <div class="form-group">
      {!! Form::label('adress', 'Dirección') !!}
      {!! Form::text('adress', null, ['class' => 'form-control', 'placeholder' => 'Nombre Completo','required' => 'true']) !!}
    </div>

    <div class="form-group">
      {!! Form::label('zone_id', 'Ciudad') !!}
      {!! Form::select('zone_id', $cities, null, ['class' => 'form-control', 'placeholder' => 'Seleccione Ciudad','required' => 'true']) !!}
    </div>

    <div class="form-group">
      {!! Form::label('need_id', 'Necesidad') !!}
      {!! Form::select('need_id[]', $needs, null, ['class' => 'form-control', 'multiple' ,'placeholder' => 'Seleccione Necesidad','required' => 'true']) !!}
    </div>

    <div class="form-group">
      {!! Form::submit('Registrar', ['class' => 'btn btn-primary']) !!}
    </div>
  {!! Form::close() !!}
@endSection