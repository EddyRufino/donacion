<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});
Route::get('proce', 'CategoriesController@procedure');
Route::group(['prefix' => 'admin'], function(){
    Route::resource('users', 'UsersController');

    Route::resource('categories', 'CategoriesController');

    Route::resource('cities', 'CitiesController');

    Route::resource('affectedarea', 'AffectedAreaController');

    Route::resource('needs', 'NeedsController');
});

// Route::get('admin/auth/login', 'Auth\AuthController@getLogin');
// Route::post('admin/auth/login', 'Auth\AuthController@postLogin');
// Route::get('admin/auth/login', 'Auth\AuthController@getLogout');



Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
